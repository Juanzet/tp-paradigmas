﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public abstract class Character
    {
        public int Life { get; set; }
        public int Speed { get; set; }
        public int Force { get; set; }
        public int JumpMagnitude { get; set; }
        public bool IsFalling { get; set; }
        public bool HasJump { get; set; }
        public bool Right { get; set; }
        public bool Left { get; set; }

        public abstract int Move(int x);

        public abstract int Jump(int y);

    }
}
