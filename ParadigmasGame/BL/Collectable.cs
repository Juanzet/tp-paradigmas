﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class Collectable
    {
        public int Score { get; set; }
        public int ElementosRecolectados { get; set; }

    }
}
