﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public class Player : Character
    {
        public Player()
        {
            Speed = 5;
            JumpMagnitude = 8;
            Force = 0;
            Right = false;
            Left = false;
        }


        public int Fall(int y)
        {
            if (IsFalling)
            {
                y += Force;
            }

            return y;
        }


        public override int Move(int x)
        {
            if (Right)
                x += Speed;
            else if(Left)
                x -= Speed;
            return x;
                
        }

        

        public override int Jump(int y)
        {
            y += JumpMagnitude;
            return y;
        }

    }
}
