﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BL;

namespace ParadigmasGame
{
    public partial class Form1 : Form,Accionable
    {
        Player mPlayer = new Player();
        Coin mCoin = new Coin();
        Enemy mEnemy = new Enemy();

        

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void MainGameTimerEvent(object sender, EventArgs e) // tiene la misma funcion que el Update en unity, hace que los elmentos se actualicen cada 1 seg
        {

            txtScore.Text = "Score: " + mCoin.Score;

            player.Top += mPlayer.JumpMagnitude;

            

            if (mPlayer.Right)
            {
                player.Left += mPlayer.Move(1);
            }

            if (mPlayer.Left)
            {
                player.Left += mPlayer.Move(1);
            }

            if(mPlayer.IsFalling && mPlayer.Force < 0)
            {
                mPlayer.IsFalling = false;
            }

            if (mPlayer.IsFalling)
            {
                mPlayer.JumpMagnitude = -8;
                mPlayer.Force = -1;
            }
            else
            {
                mPlayer.JumpMagnitude = 5;
            }

            foreach (Control x in this.Controls)
            {
                if (x is PictureBox)
                {
                    if ((string)x.Tag == "platform") // si el jugador toca el tag plataforma lo impulsa para arriba y evita que se caiga(hace el trabajo del collider)
                    {
                        if (player.Bounds.IntersectsWith(x.Bounds))
                        {
                            mPlayer.Force = 8;
                            player.Top = x.Top - player.Height;
                            mPlayer.IsFalling = false;
                        }
                        x.BringToFront();
                       
                    }


                    PickCoin();
                    //if ((string)x.Tag == "coin")
                    //{
                    //    if (player.Bounds.IntersectsWith(x.Bounds) && x.Visible == true)
                    //    {
                    //        x.Visible = false;
                    //        mCoin.Score++;
                    //    }
                    //}

                    if ((string)x.Tag == "enemy")
                    {
                        enemyTwo.Left += mEnemy.Move(mEnemy.Speed);
                        enemyOne.Left += mEnemy.Move(mEnemy.Speed);

                        if (player.Bounds.IntersectsWith(x.Bounds))
                        {
                            gameTimer.Stop();
                            txtScore.Text = "Score: " + mCoin.Score + Environment.NewLine + "You were killeed in your journey!!";
                            //Hacer que pierda el player

                        }
                    }

                    if ((string)x.Tag == "aiChangeDirection")
                    {
                        if (enemyTwo.Bounds.IntersectsWith(x.Bounds))
                        {
                            mEnemy.Speed *= -1;
                        }
                        if (enemyOne.Bounds.IntersectsWith(x.Bounds))
                        {
                            mEnemy.Speed *= -1;
                        }
                    }


                    if(player.Top + player.Height > this.ClientSize.Height + 50)
                    {
                        gameTimer.Stop();
                        txtScore.Text = "Score: " + mCoin.Score + Environment.NewLine + "You fell to your death!";
                    }


                }
            }

        }

        private void KeyIsDown(object sender, KeyEventArgs e) // al precionar un boton
        {
            if(e.KeyCode == Keys.D)
            {
                mPlayer.Right = true;
               
            }

            if (e.KeyCode == Keys.A)
            {
                mPlayer.Left = true;
                
            }


            if(e.KeyCode == Keys.Space && !mPlayer.IsFalling && mPlayer.Force>0)
            {
                player.Top -= 100;
                mPlayer.IsFalling = true;
            }


        }

        private void KeyIsUp(object sender, KeyEventArgs e) // al soltar el boton
        {
            if (e.KeyCode == Keys.D)
            {
                mPlayer.Right = false;
<<<<<<< Updated upstream

            }

=======

            }

>>>>>>> Stashed changes
            if (e.KeyCode == Keys.A)
            {
                mPlayer.Left = false;

            }
        }

        private void RestartGame() // si el jugador pierde pone todos los valroes por defecto
        {
            txtScore.Text = "Score: " + mCoin.Score.ToString();

            #region verificar si los coins son visibles
            foreach (Control x in this.Controls) 
            {
                if(x is PictureBox && x.Visible == false)
                {
                    x.Visible = true;
                    
                }
            }
            #endregion // verifica si los coins son visibles y sino, lso hace visibles
            gameTimer.Start();
        }
        public void PickCoin()
        {
            foreach(Control x in this.Controls)
            {
                if( x is PictureBox)
                {
                    if ((string)x.Tag == "coin")
                    {
                        if (player.Bounds.IntersectsWith(x.Bounds) && x.Visible == true)
                        {
                            x.Visible = false;
                            mCoin.Score++;
                        }
                    }
                }
            }
        }

        public void Actionable()
        {
            foreach (Control x in this.Controls)
            {
                if (x is PictureBox)
                {
                    if(player.Bounds.IntersectsWith(door.Bounds) && mCoin.Score == 31)
                    gameTimer.Stop();
                    txtScore.Text = "Score: " + mCoin.Score + Environment.NewLine + "Your queest is complete";
                }

            }
        }    
    }

    
}
